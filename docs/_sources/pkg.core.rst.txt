pkg.core package
================

Module contents
---------------

.. automodule:: pkg.core
   :members:
   :undoc-members:
   :show-inheritance:
