# -*- encoding: utf-8 -*-

"""Class Exceptions - specifically built for this Module"""

import warnings

class MyException(Exception):
    """Some custom Exception
    You can also add an extended message to your Exception

    :type  extended_message: str
    :param extended_message: An Extended Message to be Printed when Error is Raised
    """
    def __init__(self, extended_message : str):
        self.extended_message = extended_message

    def __str__(self):
        """Output is handled by this special Function"""
        return self.extended_message

class MyWarning(Warning):
    """Some custom Warning"""