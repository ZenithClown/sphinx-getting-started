# -*- encoding: utf-8 -*-

"""Some Common Functionalities"""

class Person:
    """A Class Describing a Person

    :type  first_name: str
    :param first_name: First Name of the Person

    :type  last_name: str
    :param last_name: Last Name of the Person
    """
    def __init__(self, first_name, last_name):
        """Creates the Variables associated with the Class"""
        self.first_name = first_name
        self.last_name  = last_name

    @property
    def full_name(self):
        """Generate Full Name of the Person"""
        return f'{self.first_name} {self.last_name}'