# -*- encoding: utf-8 -*-

"""A sub-module which can be used for various Functionalities - like
for invoking all/required init-time options (as used in pandas),
for defining global parameters, which can also be done in config.py,
and many more!
"""

from ..core import *
from ..commons import *
from ..exceptions import *