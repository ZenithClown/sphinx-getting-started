# -*- encoding: utf-8 -*-

"""Some Core Level Functionalities"""

import random
from string import ascii_uppercase

def UID(size : int = 15, hyphen : bool = True):
    """A Simple Class that generates Unique ID

    :param size:   Size of the UID
    :param hyphen: Place hyphens at 3rd, 5th and 9th Position
    if the size if less than the hyphen position, then those positions are ignored
    """
    _choice   = ascii_uppercase + '0123456789'
    _hyphenat = [i for i in [3, 2, 4] if i < size]

    _UID = ''.join([random.choice(_choice) for _ in range(size)])

    if not hyphen:
        return _UID

    UID = ''
    for pos in _hyphenat: # include hyphens
        UID += _UID[:pos] + '-'
        _UID = _UID[pos:]

    UID += _UID

    return UID