pkg package
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pkg.api
   pkg.commons
   pkg.core
   pkg.exceptions

Module contents
---------------

.. automodule:: pkg
   :members:
   :undoc-members:
   :show-inheritance:
