pkg.exceptions package
======================

Module contents
---------------

.. automodule:: pkg.exceptions
   :members:
   :undoc-members:
   :show-inheritance:
