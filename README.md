<div align="center">

<img src = "./assets/sphinxheader.png"/>
<h1 align = "center">Getting Started with Python-Code Documentations using SPHINX</h1>

<a href = "https://www.linkedin.com/in/dpramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/linkedin.svg"/></a>
<a href = "https://github.com/ZenithClown"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/github.svg"/></a>
<a href = "https://gitlab.com/ZenithClown/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/gitlab.svg"/></a>
<a href = "https://www.researchgate.net/profile/Debmalya_Pramanik2"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/researchgate.svg"/></a>
<a href = "https://www.kaggle.com/dPramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/kaggle.svg"/></a>
<a href = "https://app.pluralsight.com/profile/Debmalya-Pramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/pluralsight.svg"/></a>
<a href = "https://stackoverflow.com/users/6623589/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/stackoverflow.svg"/></a>
<a href = "https://www.hackerrank.com/dPramanik"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/hackerrank.svg"/></a>

</div>

<div align="justify">

<p align = "justify"><b>Sphinx</b> is a documentation generator written and used by the Python community. It is written in Python, and also used in other environments, <a href = "https://www.sphinx-doc.org/en/master/">(more information)</a>. In this repository, a quick-start guide for sphinx is explained - including (i) how to format your code to automatically generate codes, (ii) minimum commands required on getting started, and others.</p>

<p align = "justify">If you are using python using an <i>anaconda</i> distributions, then sphinx is already installed and you do not need to worry, else you can <code>pip install sphinx</code>. Apart from this, you might need to install the themes which can be done via <code>pip install sphinx_rtd_theme</code>.</p>

<p align = "justify"><b>Note:</b> for using sphinx, you must note that all the codes should be <i>self-contained</i>, i.e. no codes should be outside a <code>class, method or function</code>. For instance:</p>

```python
# -*- encoding: utf-8 -*-

"""This is where the module-level documentations goes"""

import pkg # import any package

print("Hello World") # this will be executed by sphinx

def myFunctions():
    pass
```

<p align = "justify">In the above code, the command <code>print("Hello World")</code> will be executed by the sphinx document generator, and this must be avoided, else these variables/functions will also be included into the documentations.</p>

## Quick Start
<p align = "justify">You can generate codes inside any special folders like <code>docs</code> which can be directly interpreted by <i>source-code control</i> websites like GitHub or GitLab which will automatically generate documentations page (WiKi) for your module, or you can use modify it in any other way! In my case, I use the directory <code>sphinx-docs</code> to generate and process the files, and move the generated codes from <code>sphinx-docs/_build/html/</code> using <i>rsync</i> so that the documentations can be generated easily!</p>

```bash
d@dPramanik:~/sphinx-getting-started$ mkdir docs sphinx-docs
d@dPramanik:~/sphinx-getting-started$ cd sphinx-docs
d@dPramanik:~/sphinx-getting-started/sphinx-docs$ sphinx-quickstart
Welcome to the Sphinx 3.1.2 quickstart utility.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.
> Separate source and build directories (y/n) [n]:

The project name will occur in several places in the built documentation.
> Project name: Getting Started with SPHINX Document Generator
> Author name(s): Debmalya Pramanik
> Project release []: 0.0.1

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see
https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
> Project language [en]: 

Creating file ~/sphinx-getting-started/sphinx-docs/source/conf.py.
Creating file ~/sphinx-getting-started/sphinx-docs/source/index.rst.
Creating file ~/sphinx-getting-started/sphinx-docs/Makefile.
Creating file ~/sphinx-getting-started/sphinx-docs/make.bat.

Finished: An initial directory structure has been created.

You should now populate your master file /home/sphinx-docs/source/index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
   make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.

# Since we have used a seperate directory, we have to let sphinx know the source directory
# Also, set the theme. These can be done from conf.py
d@dPramanik:~/sphinx-getting-started/sphinx-docs$ nano conf.py
```

<p align = "justify">Now, opening the file with your favourite editor, you need to specify the following:</p>

```python
import os  # Uncomment
import sys # Uncomment
sys.path.insert(0, os.path.abspath('..')) # Uncomment and insert the root-path

html_theme = 'sphinx_rtd_theme' # Use the rtd-theme, if not installed use pip to install

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary'
]
```

<p align = "justify">If building an module, then the root path is the one where the <code>__init__.py</code> exits, else point to the directory where all the code/function files are present. Now, we have to let sphinx know to include documentations for the sub-modules we have to update the <i>index.rst</i> and include the command <code>modules</code> as below.</p>

```rst
.. Getting Started with SPHINX Document Generator documentation master file, created by
   sphinx-quickstart on Sun Sep 13 10:06:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Getting Started with SPHINX Document Generator's documentation!
==========================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pkg.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

```

<p align = "justify">Finally, sphinx is ready to start building your documentations using <code>sphinx-apidoc -o . ../pkg/</code>. Note that the <code>pkg.rst</code> file along with other rst file per submodule is generated via this command.</p>

```bash
d@dPramanik:~/sphinx-getting-started/sphinx-docs$ sphinx-apidoc # Lists out the Usage
usage: sphinx-apidoc [OPTIONS] -o <OUTPUT_PATH> <MODULE_PATH> [EXCLUDE_PATTERN, ...]
sphinx-apidoc: error: the following arguments are required: module_path, exclude_pattern, -o/--output-dir

# thus, we can generate build files as:
d@dPramanik:~/sphinx-getting-started/sphinx-docs$ sphinx-apidoc -o . ../pkg/
Creating file ./pkg.rst.
Creating file ./pkg.api.rst.
Creating file ./pkg.commons.rst.
Creating file ./pkg.core.rst.
Creating file ./pkg.exceptions.rst.
Creating file ./modules.rst.

d@dPramanik:~/sphinx-getting-started/sphinx-docs$ make html # Generates the documentation HTML Version

# You can now view the generated documentations:
d@dPramanik:~/sphinx-getting-started/sphinx-docs$ firefox _buld/html/index.html
```

<p align = "justify">Now you can move the contents to docs, and publish your documentations online.</p>

```bash
d@dPramanik:~/sphinx-getting-started$ cd docs
d@dPramanik:~/sphinx-getting-started/docs$ rsync -rt ../sphinx-docs/_build/html/ .
d@dPramanik:~/sphinx-getting-started/docs$ touch .nojekyll # Create this file, as jekyll is not used
```

<p align = "justify">If you are on GitHub, then you can directly go to <i>Repository Settings - GitHub Pages</i> and point to the <code>master/docs</code> branch and the new website will be automatcially created. However in GitLab you need to create a special file with the contents as below.</p>

```bash
.gitlab-ci.yml  # Special File Name, with its content
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r docs/* .public # Copy contents from docs/ to public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master


# More at: https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#add-gitlab-ci
```

<p align = "justify">Finally, you can see the built documentations at <a href = "https://zenithclown.gitlab.io/sphinx-getting-started/">pkg documentations</a>.</p>

</div>
